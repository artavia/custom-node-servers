
const url = require('url'); // comment out EARL if no need exists but at your peril... muahaha
const http = require('http');
const hostname = '127.0.0.1';
const port = process.env.npm_package_config_port; // for use with package.json
// const port = 8675;

let headerval = ''; 
let contentobject = {};
let responsecode = undefined;
let encoding = null; 

const server = http.createServer( ( req , res ) => {
  // -----------------------------
  // | ~ DEFAULT settings phase
  headerval = 'text/plain'; 
  encoding = 'utf-8';

  // -----------------------------
  // | ~ PATH assignment phase
  // ----------------------------- 
  // comment out EARL here...
  req.requrl = url.parse( req.url , true); // console.log( "url.parse( req.url )  " +  url.parse( req.url ) );
  let path = req.requrl.pathname; // console.info( "path:  " +  path ); 

  if( path === '/' || path === '/index' ){ // comment out EARL here...
    responsecode = 200;
    headerval = 'text/html'; 
    contentobject = { 'Content-Type': headerval }; 
    res.writeHead( responsecode , contentobject ); 
    // console.log( "IT's on, bebe! " , __dirname + path ); 
    res.write('es6 http server says Hello\n');
    res.end();
  } // comment out EARL here...

} );

server.listen( port, hostname, ()=>{
  console.log( `es6 http server running at http://${hostname}:${port}/` );
});

// https://nodejs.org/en/about/
// open http://127.0.0.1:3002