
// app.js

const fs = require('fs');
const url = require('url'); // comment out EARL if no need exists but at your peril... muahaha
// EARL also necesitates .ico... cull the EARL call and you cull the .ico derrps and misses...

let headerval = ''; 
let contentobject = {};
let responseWithHeaders = null;
let responsecode = undefined;
let encoding = null; 

// let filename = '/path/to/incorrectDest/text.txt';
// let filename = './data/dummydata.js'; 
let filename = './index.html'; 

// ----------------------------------------------------------------------
// | operations unique to the program
// ----------------------------------------------------------------------
function CustomImpertinentDebuggers( req, res ){ 
  // console.log( "req" , req ); // search for ~ rawHeaders
  // console.log( "req.headers" , req.headers ); 
  // console.log( "req.headers['user-agent']" , req.headers['user-agent'] ); 
  
  // console.log( "req.rawHeaders" , req.rawHeaders );

  // console.log( "JSON.stringify( req.headers, null, 1 ) ", JSON.stringify( req.headers, null, 1 ) ); 

  // console.log( "res" , res ); // search for ~ HTTP/1.1 200
  // console.log( "res._header" , res._header ); // search for ~ HTTP/1.1 200

} // END CustomImpertinentDebuggers()

function CustomRender( filename, res ){ 

  fs.readFile( filename , null, ( derrp, message ) => { 
    if( derrp ) { 
      console.error( "derrp.stack" , derrp.stack ); 
      responsecode = 404;
      res.writeHead( responsecode ); 
      res.write( 'Error reading the file at ' + filename + ' not found. Sorry, Charlie!' );
    }
    else {
      // console.log( message.toString() );
      res.write( message ); // YES 
    }
    res.end(); 
  } );

} // END CustomRender()

function CustomRouteAssignment( path, res ){

  // -------------------------------------
  // | node http router or project or example specific operations
  // -------------------------------------

  if( path === '/' || path === '/index' ){ 
    responsecode = 200;
    headerval = 'text/html'; 
    contentobject = { 'Content-Type': headerval }; 
    res.writeHead( responsecode , contentobject ); 
    console.log( "index " , __dirname + path );
    CustomRender( "./index.html" , res );
  }
  else
  /* if( path === '/login' ){
    responsecode = 200;
    headerval = 'text/html'; 
    contentobject = { 'Content-Type': headerval }; 
    res.writeHead( responsecode , contentobject ); 
    console.log( "login " , __dirname + path );
    CustomRender( "./login.html" , res );
  }
  else */
  if (/\.(html)$/.test( path )){
    responsecode = 200;
    headerval = 'text/html'; 
    contentobject = { 'Content-Type': headerval }; 
    res.writeHead( responsecode , contentobject ); 
    console.log( "html " , __dirname + path );
    CustomRender( __dirname + path , res );
  } 
  // -------------------------------------
  // | node http router extension-agnostic operations
  // -------------------------------------
  else
  if (/\.(manifest.json)$/.test( path )){
    responsecode = 200;
    headerval = 'application/manifest+json'; 
    contentobject = { 'Content-Type': headerval }; 
    res.writeHead( responsecode , contentobject ); 
    console.log( "cache manifest " , __dirname + path );
    CustomRender( __dirname + path , res );
  }
  else
  if (/\.(appcache)$/.test( path )){
    responsecode = 200;
    headerval = 'text/cache-manifest'; 
    contentobject = { 'Content-Type': headerval }; 
    res.writeHead( responsecode , contentobject ); 
    console.log( "appcache " , __dirname + path ); 
    CustomRender( __dirname + path , res );
  } 
  else
  /**/
  // -------------------------------------
  // | node http router extension-agnostic operations
  // -------------------------------------
  // -------------------------------------
  // |  ~ application extensions
  // -------------------------------------
  if (/\.(json)$/.test( path )){
    responsecode = 200;
    headerval = 'application/json'; 
    contentobject = { 'Content-Type': headerval }; 
    res.writeHead( responsecode , contentobject ); 
    console.log( "json " , __dirname + path ); 
    CustomRender( __dirname + path , res );
  } 
  else
  if (/\.(rss)$/.test( path )){
    responsecode = 200;
    headerval = 'application/rss+xml'; 
    contentobject = { 'Content-Type': headerval }; 
    res.writeHead( responsecode , contentobject ); 
    console.log( "rss " , __dirname + path ); 
    CustomRender( __dirname + path , res );
  } 
  else
  if (/\.(xml)$/.test( path )){
    responsecode = 200;
    headerval = 'application/xml'; 
    contentobject = { 'Content-Type': headerval }; 
    res.writeHead( responsecode , contentobject ); 
    console.log( "xml " , __dirname + path );
    CustomRender( __dirname + path , res );
  }
  else
  if (/\.(js)$/.test( path )){
    responsecode = 200;
    headerval = 'application/javascript'; 
    contentobject = { 'Content-Type': headerval }; 
    res.writeHead( responsecode , contentobject ); 
    console.log( "js " , __dirname + path );
    CustomRender( __dirname + path , res );
  }
  else
  /**/
  // -------------------------------------
  // |  ~ text extensions
  // -------------------------------------
  if (/\.(css)$/.test( path )){
    responsecode = 200;
    headerval = 'text/css'; 
    contentobject = { 'Content-Type': headerval }; 
    res.writeHead( responsecode , contentobject ); 
    console.log( "css " , __dirname + path );
    CustomRender( __dirname + path , res );
  } 
  else
  /**/
  // -------------------------------------
  // |  ~ image extensions
  // -------------------------------------
  // -------------------------------------
  // | ~ .ico ~ uncomment if you have an ico
  //   otherwise, you can expect a big fat DERRP as a response
  // -------------------------------------
  // if (/\.(ico)$/.test( path )){
  //   responsecode = 200;
  //   headerval = 'image/x-icon'; 
  //   contentobject = { 'Content-Type': headerval }; 
  //   res.writeHead( responsecode , contentobject ); 
  //   console.log( "ico " , __dirname + path );
  //   CustomRender( __dirname + path , res );
  // }
  // else

  if (/\.(svgz?)$/.test( path )){
    responsecode = 200;
    headerval = 'image/svg+xml'; 
    contentobject = { 'Content-Type': headerval }; 
    res.writeHead( responsecode , contentobject ); 
    console.log( "svg " , __dirname + path );
    CustomRender( __dirname + path , res );
  }
  else
  if (/\.(webp)$/.test( path )){
    responsecode = 200;
    headerval = 'image/webp'; 
    contentobject = { 'Content-Type': headerval }; 
    res.writeHead( responsecode , contentobject ); 
    console.log( "webp " , __dirname + path );
    CustomRender( __dirname + path , res );
  }
  else
  if (/\.(png)$/.test( path )){
    responsecode = 200;
    headerval = 'image/png'; 
    contentobject = { 'Content-Type': headerval }; 
    res.writeHead( responsecode , contentobject ); 
    console.log( "png " , __dirname + path );
    CustomRender( __dirname + path , res );
  }
  else
  if (/\.(jpe?g)$/.test( path )){
    responsecode = 200;
    headerval = 'image/jpeg'; 
    contentobject = { 'Content-Type': headerval }; 
    res.writeHead( responsecode , contentobject ); 
    console.log( "jpg " , __dirname + path );
    CustomRender( __dirname + path , res );
  }
  else
  if (/\.(gif)$/.test( path )){
    responsecode = 200;
    headerval = 'image/gif'; 
    contentobject = { 'Content-Type': headerval }; 
    res.writeHead( responsecode , contentobject ); 
    console.log( "gif " , __dirname + path );
    CustomRender( __dirname + path , res );
  }
  else
  if (/\.(bmp)$/.test( path )){
    responsecode = 200;
    headerval = 'image/bmp'; 
    contentobject = { 'Content-Type': headerval }; 
    res.writeHead( responsecode , contentobject ); 
    console.log( "bmp " , __dirname + path );
    CustomRender( __dirname + path , res );
  }
  else
  /**/
  // -------------------------------------
  // |  ~ font extensions
  // -------------------------------------
  if (/\.(woff)$/.test( path )){
    responsecode = 200;
    headerval = 'application/font-woff'; 
    contentobject = { 'Content-Type': headerval }; 
    res.writeHead( responsecode , contentobject ); 
    console.log( "woff " , __dirname + path );
    CustomRender( __dirname + path , res );
  }
  else
  if (/\.(woff2)$/.test( path )){
    responsecode = 200;
    headerval = 'application/font-woff2'; 
    contentobject = { 'Content-Type': headerval }; 
    res.writeHead( responsecode , contentobject ); 
    console.log( "woff2 " , __dirname + path );
    CustomRender( __dirname + path , res );
  }
  else
  if (/\.(eot)$/.test( path )){
    responsecode = 200;
    headerval = 'application/vnd.ms-fontobject'; 
    contentobject = { 'Content-Type': headerval }; 
    res.writeHead( responsecode , contentobject ); 
    console.log( "eot " , __dirname + path );
    CustomRender( __dirname + path , res );
  }
  else
  if (/\.(ttf)$/.test( path )){
    responsecode = 200;
    headerval = 'application/x-font-ttf'; 
    contentobject = { 'Content-Type': headerval }; 
    res.writeHead( responsecode , contentobject ); 
    console.log( "ttf " , __dirname + path );
    CustomRender( __dirname + path , res );
  }
  else
  if (/\.(otf)$/.test( path )){
    responsecode = 200;
    headerval = 'font/opentype'; 
    contentobject = { 'Content-Type': headerval }; 
    res.writeHead( responsecode , contentobject ); 
    console.log( "otf " , __dirname + path );
    CustomRender( __dirname + path , res );
  }
  else
  /**/
  // -------------------------------------
  // |  ~ audio extensions ~ .mp3 MIA
  // -------------------------------------
  if (/\.(f4a|f4b|m4a)$/.test( path )){
    responsecode = 200;
    headerval = 'audio/mp4'; 
    contentobject = { 'Content-Type': headerval }; 
    res.writeHead( responsecode , contentobject ); 
    console.log( "mp4 audio " , __dirname + path );
    CustomRender( __dirname + path , res );
  }
  else
  if (/\.(oga|ogg|opus)$/.test( path )){
    responsecode = 200;
    headerval = 'audio/ogg'; 
    contentobject = { 'Content-Type': headerval }; 
    res.writeHead( responsecode , contentobject ); 
    console.log( "ogg audio " , __dirname + path );
    CustomRender( __dirname + path , res );
  }
  else
  /**/
  // -------------------------------------
  // |  ~ video extensions
  // -------------------------------------
  if (/\.(f4v|f4p|m4v|mp4)$/.test( path )){
    responsecode = 200;
    headerval = 'video/mp4'; 
    contentobject = { 'Content-Type': headerval }; 
    res.writeHead( responsecode , contentobject ); 
    console.log( "mp4 video " , __dirname + path );
    CustomRender( __dirname + path , res );
  }
  else
  if (/\.(ogg)$/.test( path )){
    responsecode = 200;
    headerval = 'video/ogg'; 
    contentobject = { 'Content-Type': headerval }; 
    res.writeHead( responsecode , contentobject ); 
    console.log( "ogg video " , __dirname + path );
    CustomRender( __dirname + path , res );
  }
  else
  if (/\.(webm)$/.test( path )){
    responsecode = 200;
    headerval = 'video/webm'; 
    contentobject = { 'Content-Type': headerval }; 
    res.writeHead( responsecode , contentobject ); 
    console.log( "webm " , __dirname + path );
    CustomRender( __dirname + path , res );
  }

  // -------------------------------------
  // |  ~ form content-types and php are in 
  //   need of further testing, distinction 
  //   and clarification among one another 
  //   because implementation is on a case by case basis
  //   depending on the data type that is being
  //   returned (e.g.-- html, json, xml, ETC.)
  // -------------------------------------
  
  //else
  //if (/\.(php)$/.test( path )){ // post submit forms
  //  responsecode = 200;
    // headerval = 'application/x-www-form-urlencoded'; 
    // headerval = 'multipart/form-data';
    // headerval = 'text/html'; 
  //  contentobject = { 'Content-Type': headerval }; 
  //  res.writeHead( responsecode , contentobject ); 
    // console.log( "x-www-form-urlencoded " , __dirname + path );
    // console.log( "form-data " , __dirname + path );
  //  CustomRender( __dirname + path , res );
  //} 

  // -------------------------------------
  // | send 404
  // -------------------------------------
  else{
    console.log( "NAY:" , __dirname + path );
    responsecode = 404;
    res.writeHead( responsecode );
    res.write("Route not defined, babe!");
    res.end();
  } // END ELSE
} // END CustomRouteAssignment()

// -----------------------------
// | Cross-origin resource timing
// | Allow cross-origin access to the timing information for all resources.
//   http://www.stevesouders.com/blog/2014/08/21/resource-timing-practical-tips/
// -----------------------------
function Custom_Enable_XO_resource_timing_Headers( res ){
  // res.setHeader( 'Timing-Allow-Origin' , 'sameorigin' ); 
  res.setHeader( 'Timing-Allow-Origin' , 'SAMEORIGIN' ); 
}
// ----------------------------------------------------------------------
// | Cross-origin requests
//   Allow cross-origin requests ~ https://developer.mozilla.org/en-US/docs/Web/HTTP/Access_control_CORS
// ----------------------------------------------------------------------
function Custom_Enable_CORS_Headers( res ){
  // The allowed origins can consist of a wildcard character: * to let every origin request the data OR it could consist of just a few origins in quantity separated by spaces. 
  // https://code.tutsplus.com/tutorials/html5-mastery-web-security--cms-24846
  // https://code.tutsplus.com/articles/client-side-security-best-practices--net-35677
  // ... BORKULA has shown up to the party... 
  // res.setHeader('Access-Control-Allow-Origin', '*'); 
  res.setHeader('Access-Control-Allow-Origin', 'https://localhost:80/ http://localhost:80/ https://127.0.0.1:80/ http://127.0.0.1:80/'); 
  res.setHeader('Access-Control-Allow-Methods', 'POST, GET, OPTIONS'); 
  res.setHeader('Access-Control-Allow-Headers', 'X-CUSTOM, Content-Type'); 
} 
// ----------------------------------------------------------------------
// | HTTP Strict Transport Security (HSTS)                              |
// | # https://blogs.msdn.microsoft.com/ieinternals/2014/08/18/strict-transport-security/ 
//   Force client-side SSL redirection.
// ----------------------------------------------------------------------
function Custom_Enable_Strict_Transport_Security_Headers( res ){ 
  res.setHeader( 'Strict-Transport-Security' , 'max-age=16070400' ); 
  // res.setHeader( 'Strict-Transport-Security' , 'max-age=16070400; includeSubDomains' ); 
}
// ----------------------------------------------------------------------
// | Content Security Policy (CSP)                                      |
// Mitigate the risk of cross-site scripting and other content-injection attacks.
// ----------------------------------------------------------------------
function Custom_Enable_Content_Security_Policy_Headers( res ){ 
  res.setHeader( 'Content-Security-Policy' , "script-src 'self'; object-src 'self'" );
  // "default-src 'none'; script-src 'self'; connect-src 'self'; img-src 'self'; style-src 'self';"  
  // "default-src 'self'; style-src 'self' fonts.googleapis.com 'unsafe-inline' ; child-src 'self'; script-src 'self'; img-src 'self' data: ; connect-src 'self'; media-src 'self'; object-src 'self'; font-src 'self' fonts.gstatic.com "
}
// ----------------------------------------------------------------------
// | Server-side technology information                                 |
// | Remove the `X-Powered-By` response header that
// | contributes to header bloat and can unnecessarily expose vulnerabilities
// ----------------------------------------------------------------------
function Custom_Disable_X_Powered_By_Headers( res ){
  // do something
  res.removeHeader( 'X-Powered-By' );
}
// ----------------------------------------------------------------------
// | Clickjacking                                                       |
// | Protect website against clickjacking
// | This needs to only apply to HTML documents and not to other resources
// | https://cure53.de/xfo-clickjacking.pdf
// | https://blogs.msdn.microsoft.com/ieinternals/2010/03/30/combating-clickjacking-with-x-frame-options/
// ----------------------------------------------------------------------
function Custom_Enable_X_Frame_Options_Headers( path, res ){
  if ( !!(/\.(html)$/.test( path )) ){
    // do something 
    res.setHeader( 'X-Frame-Options' , 'sameorigin' ); 
  }
  else
  if ( !(/\.(html)$/.test( path )) ){
    // do something else
    res.removeHeader( 'X-Frame-Options' ); 
  }
}
// ----------------------------------------------------------------------
// | Reflected Cross-Site Scripting (XSS) attacks                       |
// | The process of re-enabling the pre-built cross-site scripting (XSS) filter
// | This needs to only apply to HTML documents and not to other resources
// | https://hackademix.net/2009/11/21/ies-xss-filter-creates-xss-vulnerabilities
// | https://blogs.msdn.microsoft.com/ieinternals/2011/01/31/controlling-the-xss-filter/
// ----------------------------------------------------------------------
function Custom_Enable_X_XSS_Protection_Headers( path, res ){ 
  if ( !!(/\.(html)$/.test( path )) ){
    // do something 
    res.setHeader( 'X-XSS-Protection' , '1; mode=block' ); 
  }
  else
  if ( !(/\.(html)$/.test( path )) ){
    // do something else
    res.removeHeader( 'X-XSS-Protection' ); 
  }
} 
// ----------------------------------------------------------------------
// | Prevent some browsers from MIME-sniffing the response.             |
// | Reducing MIME type security risks
// | https://blogs.msdn.microsoft.com/ie/2008/07/02/ie8-security-part-v-comprehensive-protection/ 
// ----------------------------------------------------------------------
function Custom_Enable_X_Content_Type_Options_Headers( res ){ 
  res.setHeader( 'X-Content-Type-Options' , 'nosniff' ); 
  // res.setHeader( 'X-Content-Type-Options' , 'nosniff' ); 
}
// ----------------------------------------------------------------------
// | Document modes                                                     |
// | MICROSOFT INTERNET EXPLORER browser helper function
// | (!) Starting with Internet Explorer 11, document modes are deprecated.
// | Force IE 8/9/10 to render pages in the highest mode available 
// | in the various cases when it may not
// | This needs to only apply to HTML documents and not to other resources
// | https://msdn.microsoft.com/en-us/library/ie/bg182625.aspx#docmode
// | https://blogs.msdn.microsoft.com/ie/2014/04/02/stay-up-to-date-with-enterprise-mode-for-internet-explorer-11/
// ----------------------------------------------------------------------
function Custom_Enable_X_UA_Compatible_Headers( path, res ){ 
  if ( !!(/\.(html)$/.test( path )) ){
    // do something 
    res.setHeader( 'X-UA-Compatible' , 'IE=edge' ); 
  }
  else
  if ( !(/\.(html)$/.test( path )) ){
    // do something else
    res.removeHeader( 'X-UA-Compatible' ); 
  }
}
// ----------------------------------------------------------------------
// | Iframes cookies                                                    |
// | MICROSOFT INTERNET EXPLORER browser helper function
// | Allow cookies to be set from iframes in Internet Explorer.
// | https://msdn.microsoft.com/en-us/library/ms537343.aspx
// ----------------------------------------------------------------------
function Custom_Enable_Iframes_Cookies_Headers( res ){
  // do something
  res.setHeader( 'P3P' , "policyref=\"/w3c/p3p.xml\", CP=\"IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT\"" );
}
// ----------------------------------------------------------------------
// | Content transformation                                             |
// | PERFORMANCE helper function
// | Prevent intermediate caches or proxies (e.g.: such as the ones used
// | by mobile network providers) from modifying the website's content.
// | https://developers.google.com/speed/pagespeed/module/configuration#notransform
// | https://stackoverflow.com/questions/4480304/how-to-set-http-headers-for-cache-control
// | https://cloud.google.com/storage/docs/transcoding
// ----------------------------------------------------------------------
function Custom_Enable_Content_Transformation_Headers( res ){
  res.setHeader( 'Cache-Control' , 'private, no-cache, no-store, proxy-revalidate, no-transform' );
  res.setHeader( 'Pragma' , 'no-cache' );
}

// ----------------------------------------------------------------------
// | Compression                                                        |
// | PERFORMANCE helper function
// | Force compression for mangled `Accept-Encoding` request headers
// | Compress all output by media type
// | https://developer.yahoo.com/blogs/ydn/pushing-beyond-gzipping-25601.html
// ----------------------------------------------------------------------
function Custom_Enable_Compression_Headers( path , res ){
  // res.setHeader( '' , '' );
}
// ----------------------------------------------------------------------
// | Expires headers                                                    |
// | PERFORMANCE helper function
// | https://gist.github.com/tagr/170627486377551831de
// | https://devcenter.heroku.com/articles/increasing-application-performance-with-http-cache-headers
// ----------------------------------------------------------------------
function Custom_Enable_Expires_Headers( path , res ){

  if ( !!(/\.(html|appcache|json|xml)$/.test( path )) ){ // zero seconds
    // do something else
    // res.setHeader( 'Cache-Control' , 'private, max-age=0' );
    let secs = 0;
    let expr = 'public, max-age='+ '' + secs +'';
    res.setHeader( 'Cache-Control' , expr );
    // res.setHeader( 'Expires', new Date(Date.now() + 0 ).toUTCString() );
    res.setHeader( 'Expires', new Date(Date.now() + (1000 + secs) ).toUTCString() ); 
  }
  else
  if ( !!(/\.(rss|atom|rdf)$/.test( path )) ){ // 1 hour (86400 / 24)
    // do something else
    // res.setHeader( 'Cache-Control' , 'private, max-age=3600' );
    let secs = 3600;
    let expr = 'public, max-age='+ '' + secs +'';
    res.setHeader( 'Cache-Control' , expr );
    // res.setHeader( 'Expires', new Date(Date.now() + 3600000 ).toUTCString() );
    res.setHeader( 'Expires', new Date(Date.now() + (1000 + secs) ).toUTCString() ); 
  }
  else
  if ( !!(/\.(ico|manifest.json)$/.test( path )) ){ // 1 week (86400 * 7)
    // do something else
    // res.setHeader( 'Cache-Control' , 'private, max-age=604800' );
    let secs = 604800;
    let expr = 'public, max-age='+ '' + secs +'';
    res.setHeader( 'Cache-Control' , expr );
    // res.setHeader( 'Expires', new Date(Date.now() + 604800000 ).toUTCString() );
    res.setHeader( 'Expires', new Date(Date.now() + (1000 + secs) ).toUTCString() ); 
  }
  else
  if ( !!(/\.(woff|woff2|eot|ttf|otf|f4a|f4b|m4a|oga|ogg|opus|f4v|f4p|m4v|mp4|ogg|webm|bmp|gif|jpg|jpeg|png|svgz?|webp)$/.test( path )) ){ // 30 days (86400 * 30 )
    // do something else
    // res.setHeader( 'Cache-Control' , 'private, max-age=2592000' );
    let secs = 2592000;
    let expr = 'public, max-age='+ '' + secs +'';
    res.setHeader( 'Cache-Control' , expr );
    // res.setHeader( 'Expires', new Date(Date.now() + 2592000000 ).toUTCString() );
    res.setHeader( 'Expires', new Date(Date.now() + (1000 + secs) ).toUTCString() ); 
  }
  else
  if ( !!(/\.(js|css)$/.test( path )) ){ // 1 year (86400 * 365 )
    // do something else
    // res.setHeader( 'Cache-Control' , 'private, max-age=31536000' );
    let secs = 31536000;
    let expr = 'public, max-age='+ '' + secs +'';
    res.setHeader( 'Cache-Control' , expr );
    // res.setHeader( 'Expires', new Date(Date.now() + 31536000000 ).toUTCString() );
    res.setHeader( 'Expires', new Date(Date.now() + (1000 + secs) ).toUTCString() ); 
  }
  
}

// ----------------------------------------------------------------------
// | ETags                                                              |
// | PERFORMANCE helper function
// | Remove `ETags` as resources are sent with far-future expires headers
// | https://developer.yahoo.com/performance/rules.html#etags
// ----------------------------------------------------------------------
function Custom_Disable_ETags_Headers( res ){
  // do something
  res.removeHeader( 'ETag' );
}
// ----------------------------------------------------------------------
// | the module export anticipating any possible and optional headers to go
// ----------------------------------------------------------------------
module.exports = { 

  handleRequest: ( req, res ) => { 
    // -----------------------------
    // | ~ DEFAULT settings phase
    headerval = 'text/plain'; 
    encoding = 'utf-8';
    // -----------------------------
    // | ~ PATH assignment phase
    // ----------------------------- 
    req.requrl = url.parse( req.url , true ); // console.log( "url.parse( req.url ) " +  url.parse( req.url ) );
    let path = req.requrl.pathname; // console.info( "path:  " +  path ); 

    // -----------------------------
    // | CROSS ORIGIN helper functions 
    // | optional Cross-origin resource timing phase
    // | Allow cross-origin access to the timing information for all resources.
    //   http://www.stevesouders.com/blog/2014/08/21/resource-timing-practical-tips/
    // | optional CORS headers phase ~ https://zinoui.com/blog/cross-origin-resource-sharing
    // -----------------------------
    Custom_Enable_XO_resource_timing_Headers( res );
    Custom_Enable_CORS_Headers( res );

    // -----------------------------
    // | SECURITY helper functions 
    // | optional TLS:443 headers phase(s) ~ https://zinoui.com/blog/security-http-headers 
    // | https://wiki.mozilla.org/Security/CSP/Specification
    // -----------------------------
    // Custom_Enable_Strict_Transport_Security_Headers( res );
    
    // -----------------------------
    // | SECURITY helper functions 
    // | optional CSP headers phase(s) ~ https://zinoui.com/blog/security-http-headers 
    // | https://wiki.mozilla.org/Security/CSP/Specification
    // | and, visit an online CSP header generator at http://cspisawesome.com/ 
    // -----------------------------
    // Custom_Enable_Content_Security_Policy_Headers( res );
    
    // -----------------------------
    // | SECURITY helper functions 
    // | optional SECURITY headers phase(s) ~ https://zinoui.com/blog/security-http-headers 
    // | https://wiki.mozilla.org/Security/CSP/Specification
    // -----------------------------
    Custom_Disable_X_Powered_By_Headers( res );
    Custom_Enable_X_Frame_Options_Headers( path, res );
    Custom_Enable_X_XSS_Protection_Headers( path, res );
    Custom_Enable_X_Content_Type_Options_Headers( res );
    
    // -----------------------------
    // | SECURITY helper functions 
    // | optional INTERNET EXPLORER headers phase(s) ~ https://zinoui.com/blog/security-http-headers 
    // | https://wiki.mozilla.org/Security/CSP/Specification
    // -----------------------------    
    if ( !!(/\.(MSIE 8.0|MSIE 9.0|MSIE 10.0)$/.test( req.headers['user-agent'] )) ){
      // do something else
      Custom_Enable_X_UA_Compatible_Headers( path, res );
      Custom_Enable_Iframes_Cookies_Headers( res ); 
    }
    // ------------------------------
    // | PERFORMANCE helper functions 
    // | Content transformation       
    // | Compression -- in progress (ante-penultimate item before tls)
    // | Expires headers
    // | ETags 
    // ------------------------------
    Custom_Enable_Content_Transformation_Headers( res );
    // Custom_Enable_Compression_Headers( path , res );
    Custom_Enable_Expires_Headers( path , res );
    Custom_Disable_ETags_Headers( res );

    // -----------------------------
    // | ~ FINAL PATH response building phase
    // -----------------------------    
    CustomRouteAssignment( path, res );
    // CustomImpertinentDebuggers( req , res );
  }
  
}; // END module