
## Description
A starter seed structure for nodejs __http__ and __https__ servers designed to go on accompany your project setup of choice as an alternative to other popular servers.

## Download a copy today
This is still open sourced code so may it is not so different after all from previous work. As you can see, this project is only for download on your own personal computer of choice and it is not for exhibition at github.

## You only need nodejs to run &lsquo;em!
All of these constitute personal seed&#45;servers for rapid opinionated deployment. You will find that the focus is on each the [http](https://nodejs.org/api/http.html "node http server") and [https](https://nodejs.org/api/https.html "node https server") servers that come included in nodejs. It used to be that you would find the obligatory __hello world__ app at the nodejs index page but these days its expressed in ES6 and found [elsewhere](https://nodejs.org/en/about/ "node hello world example"). This project expands on hello world&hellip; big time! 

## Caveat-emptor
If you want to play with the secured-server you can use the exact same self&#45;signed key and cert you have on your local pc for use with another server such as Apache, for example. If you do not have them, you will need to generate your own __self&#45;signed request__ in order to produce each the key and the certification. Again, the aforementioned __is outside of the scope__ of this project. 

## But really... What else is needed?
No downloads are necessary because they are ready to go out of the box! For each __http__ and __https__ servers, you will find a test application in order to begin to satisfy your intellectual curiosity. Then, for each http and https you will find __app.js__ and __app-secured.js__ which will each invoke the fully functioning servers that can be further customized to your liking. Instructions are located down below.

## Other Background
This project did not have any single source of creative inspiration. Rather, the arrangements that you see are a result of different ideas and implementations. Plus, those ideas on their own never fully addressed what I wanted and needed to accomplish. Now, I want to publish what I wish would have been available when I was starting off. 

## Running instructions and Methodology
Here are some suggestions on how to create a local instance presuming that you have each __Node &amp; NPM__ already installed. You will download my project, then, get familiarized with &laquo;npm run [custom_name]&raquo; before you lift off! 

Let&rsquo;s get started:
- Download the contents &amp; place them in your present working directory (or __*pwd*__ for short);
- There are a few custom directives in the __*package.json*__ file under the __*scripts*__ subsection. In no particular order, you can fire off each command to produce a different result.

## Here are some initial testing directives
This task is a generic statement to test the http server!
```sh
$ npm run http
```

This task is an identical statement to the one above which will merely test the https server! Run this only if you procured the self&#45;signed request, then, you can __edit the paths__ contained to accomodate your own local setup.
```sh
$ npm run https
```

## Here are directives to run a website
This is a console statement that initializes the http server!
```sh
$ npm start
```


This is a console statement that initializes the secured https server! Again, if you composed a self&#45;signed request, then, you can run this without any foreseeable problems.
```sh
$ npm run secured
```

### Reading list
These are some items that helped me over time gather the material and ideas necessary to complete this in the manner it was done. And, I would like to thank all of the distinct authors and/or contributors who have helped me indirectly along this journey:
- __&laquo; Simon Holmes &raquo;__ Simon has catered a set of files that helped me found at his [github repo](https://github.com/simonholmes/knowing-node "link to knowing node"). Thanks, man!
- __&laquo; Google engineers (including Paul Irish and Sam Dutton) &raquo;__ At some point, you should consult the [h5bp](https://github.com/h5bp/ "link to h5bp") repo to get the creative juices flowing. Also, the course on PWAs by Sam Dutton and Ms. Sara (I didn&rsquo;t catch the surname&#45;&#45; my apologies), especially the chapter on the fetch API which showcases each a CORS and non-CORS server implementation, might be of some help, too. Visit the repo for [Your first PWA course](https://github.com/googlecodelabs/your-first-pwapp/ "link to googlecodelabs repo"). I thank you all. 
- __&laquo; Academind (Max Schwarz Mueller &amp; company) &raquo;__ I love Maxxie! He has some great tutorials found at [youtube](https://www.youtube.com/channel/UCSJbGtTlrDami-tDGPUV9-w/videos "link to academind videos"). You can find a large selection of mini-courses that are pertinent and will get you on your way. The [NodeJs course](https://www.youtube.com/watch?v=65a5QQ3ZR2g&list=PL55RiY5tL51oGJorjEgl6NVeDbx_fO5jR "link to nodejs course") was instrumental in helping me expand what I had learned previously. Max will get you started. The rest is up to you or me.


## That&rsquo;s it!
You can now open your default browser which should show off the fruits of your labor. Have fun!