
const url = require('url'); // comment out EARL if no need exists but at your peril... muahaha
// EARL also necesitates .ico... cull the EARL call and you cull the .ico derrps and misses...

const https = require('https');
const hostname = '127.0.0.1';
const port = process.env.npm_package_config_secport; // for use with package.json
// const port = 8443;

let headerval = ''; 
let contentobject = {};
let responsecode = undefined;
let encoding = null; 

const pathtokey = '/PATH/to/selfsigned.key';
const pathtocert = '/PATH/to/selfsigned.crt'; 

const fs = require('fs');

const options = {
  key: fs.readFileSync( pathtokey ) 
  , cert: fs.readFileSync( pathtocert ) 
};

const server = https.createServer( options , ( req , res ) => {
  // -----------------------------
  // | ~ DEFAULT settings phase
  headerval = 'text/plain'; 
  encoding = 'utf-8';

  // -----------------------------
  // | ~ PATH assignment phase
  // ----------------------------- 
  // comment out EARL here...
  req.requrl = url.parse( req.url , true); // console.log( "url.parse( req.url )  " +  url.parse( req.url ) );
  let path = req.requrl.pathname; // console.info( "path:  " +  path ); 

  if( path === '/' || path === '/index' ){ // comment out EARL here...
    responsecode = 200;
    headerval = 'text/html'; 
    contentobject = { 'Content-Type': headerval }; 
    res.writeHead( responsecode , contentobject ); 
    // console.log( "IT's on, bebe! " , __dirname + path ); 
    res.write('es6 https server says Hello\n');
    res.end();
  } // comment out EARL here...

} );

server.listen( port, hostname, ()=>{
  console.log( `es6 https server running at https://${hostname}:${port}/` );
});

// https://nodejs.org/en/about/
// open https://127.0.0.1:3002