
// -------------------------------------
// trm 1:         node server.js
// browser 2:     https://127.0.0.1:8443
// -------------------------------------

const https = require('https');
const port = process.env.npm_package_config_secport; // for use with package.json
// const port = 8443;
const ip = '127.0.0.1';
const app = require('./app-secured');

const pathtokey = '/PATH/to/selfsigned.key';
const pathtocert = '/PATH/to/selfsigned.crt'; 

const fs = require('fs');

const options = {
  key: fs.readFileSync( pathtokey ) 
  , cert: fs.readFileSync( pathtocert ) 
};

const server = https.createServer( options, app.handleRequest ); // production server

server.listen( port, ip, ()=>{
  console.log( `es6 https server running at https://${ip}:${port}/` );
} );