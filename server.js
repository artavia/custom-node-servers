
// -------------------------------------
// trm 1:         node server.js
// browser 2:     http://127.0.0.1:8675
// -------------------------------------

const http = require('http');
const port = process.env.npm_package_config_port; // for use with package.json
// const port = 8675;
const ip = '127.0.0.1';
const app = require('./app');

const server = http.createServer( app.handleRequest );
server.listen( port, ip, ()=>{
  console.log( `es6 http server running at http://${ip}:${port}/` );
} );